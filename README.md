# How to use it

## Domain
* Register a domain
	* I use https://www.freenom.com because I don't want to pay for any domain for test purposes
* Create an instance on any public cloud (any free tier instance is perfect)
* Assign a fixed External IP to your instance
* Change the nameserver on the register site to your instance's external IP

## Automatic mode on AWS or GCP

This will create and instance on aws install and configure powerdns with xip mod for your custom domain

Before doing "terraform apply" please review and configure vars.tf file for environments and custom domain settings

Pre-requisits
* Terraform installed
* Awscli installed and configured with proper credentials


```bash
git clone https://gitlab.com/Giondo/dns-xip.git
#For AWS
cd dns-xip/aws
#For GCP
cd dns-xip/gcp

terraform init
terraform apply

```

----
## Manual Mode
### install power dns
Install Power DNS server,  pipe backend and some tools to check it

```bash
apt install pdns-server pdns-backend-pipe bind-utils
```

### Clone Git from xip pipe mod

```bash
git clone https://github.com/basecamp/xip-pdns
```

### Copy files

```bash
cp xip-pdns/bin/xip-pdns /usr/local/bin/.
chown pdns.root /usr/local/bin/xip-pdns
chmod +x /usr/local/bin/xip-pdns
cp xip-pdns/etc/xip-pdns.conf.example /etc/powerdns/pdns$(YOURDOMAIN).conf
```

### include extra pipe in Power DNS
```bash
echo "launch=pipe" > /etc/powerdns/pdns.d/pipe.conf
echo "pipe-command=/usr/local/bin/xip-pdns /etc/powerdns/pdns.$(YOURDOMAIN).conf" >> /etc/powerdns/pdns.d/pipe.conf
```

### Replace domain name in the config

YOURDOMAIN=Domain
EIP=External IP

```bash
sed -i "s/^XIP_DOMAIN.*/XIP_DOMAIN=\"$DOMAIN\"/g" "/etc/powerdns/pdns.$YOURDOMAIN.conf"
sed -i "s/^XIP_ROOT_ADDRESSES.*/XIP_ROOT_ADDRESSES=( \"$EIP\" )/g" "/etc/powerdns/pdns.$YOURDOMAIN.conf"
sed -i "s/^XIP_NS_ADDRESSES.*/XIP_NS_ADDRESSES=( \"$EIP\" \"$EIP\" )/g" "/etc/powerdns/pdns.$YOURDOMAIN.conf"
```

### Test
Restart Power DNS and test

```bash
systemctl restart pdns

dig pepe.10.10.10.10.$(YOURDOMAIN) @localhost
10.10.10.10
```
the result of the dig command shoul be the IP you use in this case "10.10.10.10"
