variable "AWS_REGION" {
  default = "eu-west-1"
}

// ami-06358f49b5839867c --> Ubuntu Server 18.04 LTS (HVM), SSD Volume Type - ami-06358f49b5839867c (64-bit x86) / ami-000c356ce8d6328ef (64-bit Arm)
variable "AMIS" {
  type = "map"
  default = {
    us-east-1 = "ami-06358f49b5839867c"
    us-west-2 = "ami-06b94666"
    eu-west-1 = "ami-844e0bf7"
  }
}
variable "DOMAIN" {
  default = "logicalislabs.tk"
}
