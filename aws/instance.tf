resource "aws_instance" "powerdns" {
  ami           = "${lookup(var.AMIS, var.AWS_REGION)}"
  instance_type = "t2.micro"

  # the public SSH key
  key_name = "ansible-tower-tic"
  # the security group
  vpc_security_group_ids = ["${aws_security_group.powerdns-allow.id}"]
  # tags
  tags = {
    Name = "POWERDNS"
  }
  # user data
  user_data = "${data.template_cloudinit_config.cloudinit-example.rendered}"

}
