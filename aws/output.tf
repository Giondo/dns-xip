/*
data "template_file" "textosalida" {
  template = "${texto}"
  vars = {
    texto = "This is the external IP of your instance  \n To test if everything is working ok run  \n dig +short YOURDOMAIN @EIP."
  }
}

output "salida" {
  value = "${template_file.textosalida.rendered}"
}

*/
output "ExternalIP" {
	# value = "${aws_eip.EXTERNAL.public_ip}"
  value = "${aws_instance.powerdns.*.public_ip}"
  description = "External IP of your instance"
}
