#!/bin/bash
#Install and configure powerdns with pipe mod and custom domain
#by Giondo
#packages already installed by cloud-init check the packages section
#This script is executed with root user

exec 1> /var/log/powerdns-startup.log 2>&1
set -x
#set -ex

echo "LOG:" > /tmp/logstart.txt
AWS=${AWS}
GCP=${GCP}
TMPFOLDER="/tmp/pdns/"
SITEFOLDER="/tmp/site/"
DOMAIN=${DOMAIN}

if [ ${AWS} = "1" ]
then
  EIP=`curl http://169.254.169.254/latest/meta-data/public-ipv4`
fi

if [ ${GCP} = "1" ]
then
  apt-get update; sudo apt-get install -yq pdns-server pdns-backend-pipe git apache2;
  EIP=`curl -H "Metadata-Flavor: Google" http://169.254.169.254/computeMetadata/v1/instance/network-interfaces/0/access-configs/0/external-ip`
fi




git clone https://github.com/basecamp/xip-pdns $TMPFOLDER
cp "$TMPFOLDER/bin/xip-pdns" "/usr/local/bin/."
chown pdns.root /usr/local/bin/xip-pdns
chmod +x /usr/local/bin/xip-pdns
cp "$TMPFOLDER/etc/xip-pdns.conf.example" "/etc/powerdns/pdns.$DOMAIN.conf"


echo "launch=pipe" > /etc/powerdns/pdns.d/pipe.conf
echo "pipe-command=/usr/local/bin/xip-pdns /etc/powerdns/pdns.$DOMAIN.conf" >> /etc/powerdns/pdns.d/pipe.conf

sed -i "s/^XIP_DOMAIN.*/XIP_DOMAIN=\"$DOMAIN\"/g" "/etc/powerdns/pdns.$DOMAIN.conf"
sed -i "s/^XIP_ROOT_ADDRESSES.*/XIP_ROOT_ADDRESSES=( \"$EIP\" )/g" "/etc/powerdns/pdns.$DOMAIN.conf"
sed -i "s/^XIP_NS_ADDRESSES.*/XIP_NS_ADDRESSES=( \"$EIP\" \"$EIP\" )/g" "/etc/powerdns/pdns.$DOMAIN.conf"

##some kind of bug with simple bind backend
#so I just remove it, since its not needed
rm /etc/powerdns/pdns.d/pdns.simplebind.conf
rm /etc/powerdns/pdns.d/bind.conf

sudo systemctl restart pdns


############APACHE SITE#############
git clone https://gitlab.com/Giondo/dns-xip $SITEFOLDER
cp $SITEFOLDER/site/* /var/www/html/.
sed -i "s/giondo-infra.cf/"$DOMAIN"/g" "/var/www/html/index.html"
