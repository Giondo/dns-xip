resource "google_compute_instance" "powerdns" {
  name         = "powerdns"
  machine_type = "f1-micro"
  zone         = "us-central1-f"

  tags = ["powerdns"]

  boot_disk {
    initialize_params {
      image = "${var.IMAGE}"
    }
  }

  network_interface {
    network = "default"
    access_config {
      nat_ip = "${google_compute_address.static.address}"
    }
  }

  metadata = {
    foo = "bar"
  }

  metadata_startup_script = "${data.template_file.shell-script.rendered}"

}
