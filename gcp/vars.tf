variable "REGION" {
  default = "us-central1"
}

variable "PROJECTID" {
  default = "ocp-on-gcp-241720"
}

// ami-06358f49b5839867c --> Ubuntu Server 18.04 LTS (HVM), SSD Volume Type - ami-06358f49b5839867c (64-bit x86) / ami-000c356ce8d6328ef (64-bit Arm)
variable "IMAGE" {
  default = "debian-cloud/debian-9"
}
variable "DOMAIN" {
  default = "logicalislabs.tk"
}
