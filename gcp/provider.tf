provider "google" {
  credentials = "${file("ocp-on-gcp-terraform.json")}"
  project     = "${var.PROJECTID}"
  region      = "${var.REGION}"
}
