output "ExternalIP" {
  value = "${google_compute_instance.powerdns.network_interface.0.access_config.0.nat_ip}"
  description = "External IP of your instance"
}
