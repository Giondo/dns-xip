data "template_file" "shell-script" {
  template = "${file("../scripts/powerdns.sh")}"
  vars = {
    GCP = "1"
    AWS = "0"
    DOMAIN = "${var.DOMAIN}"
  }
}
/*
data "template_cloudinit_config" "cloudinit-example" {

  gzip = false
  base64_encode = false

  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.shell-script.rendered}"
  }

}
*/
